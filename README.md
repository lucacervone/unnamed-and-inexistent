# The (still) Unnamed and (still) Inexistent 

This project is about creating a decentralised system for Universal Basic Income and Pension System for independent creators of Open Source Commons. 

## 1. Summary

This project, purposefully, does not (still) have a name and it is (still) inexistent. It will be created in a collaborative and Open Source manner by starting by its very birth. The only things that I will (humbly) attempt to supply are the [abstract](#Abstract) in the following section and a [proposal for a few next steps](#proposal-for-next-steps). For this project I will create a Gitcoin grant. Eventual funds received will be used up to my discretion until the governance is established (as in [point 2 of section 3.](#proposal-for-next-steps)), thoug, mostly to attempt to involve people in this project. 

## 2. Abstract

As an in independent creator of Open Source commons (mostly software in my own case), I would like to challenge the current status quo by demonstrating that it is possible to create s Universal Basic Income System and a Pension System for independent creators of Open Source Commons without leveraging on centralised Institutions, States or Companies. 

The idea is quite simple and can be described as the followings:

- Creators can group in communities; 
- a creator can belong to more than one community;
- creators choose the percentage on their earnings that they want to share with communities they belong to;
- total incomings of a community are divided into funds allocated for UBI and funds allocated for PS;
- creators get their split of funds allocated for UBI on a daily base;
- creators get their split of funds allocated for PS when they want to.


## 3. Proposal For Next Steps

1. collect people interested in working on this project
2. define the distributed governance of this project
3. choose a name for the project
4. iterate this file toward a green paper


## 4. How to be involved in the project

- The project will be associated to a Gitcoin grant (as in a "pre-funding" stage); 
- As soon as I have a first funder, I'll create a GitHub project on this (or better a GitLab project); 
- Every person or company that funds the project in this stage will have access to the project;

## 5. Conclusions

I've no idea about how things will evolve, but hope this will be an interesting and engaging experiment for everyone.
